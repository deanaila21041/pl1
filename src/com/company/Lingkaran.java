package com.company;

public class Lingkaran {
    public double jari_jari;
    public double diameter;
    private double phi = 3.14;
    private double luas;
    private double keliling;

    public double getLuas() {
        luas = phi * jari_jari * jari_jari;
        return luas;
    }

    public double getKeliling() {
        keliling = 2 * phi * jari_jari;
        return keliling;
    }

    public double getDiameter() {
        diameter = 2 * jari_jari;
        return diameter;
    }
}
