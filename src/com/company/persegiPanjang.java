package com.company;

public class persegiPanjang {
    public double panjang;
    public double lebar;
    private double diagonal;
    private double luas;
    private double keliling;

    public double getDiagonal() {
        diagonal = Math.sqrt(panjang * panjang + lebar * lebar);
        return diagonal;
    }

    public double getLuas() {
        luas = panjang * lebar;
        return luas;
    }

    public double getKeliling() {
        keliling = 2 * (panjang + lebar);
        return keliling;
    }
}
