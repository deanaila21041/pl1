package com.company;

public class mainObjek1 {
    public static void main(String[] args) {
        Lingkaran objekLingkaranKepala = new Lingkaran();
        objekLingkaranKepala.jari_jari = 10;
        objekLingkaranKepala.diameter = 20;

        persegiPanjang objekPersegiPanjangBadan = new persegiPanjang();
        objekPersegiPanjangBadan.panjang = 30;
        objekPersegiPanjangBadan.lebar = 25;

        persegiPanjang objekPersegiPanjangTanganKiri = new persegiPanjang();
        objekPersegiPanjangTanganKiri.panjang = 25;
        objekPersegiPanjangTanganKiri.lebar = 5;

        persegiPanjang objekPersegiPanjangTanganKanan = new persegiPanjang();
        objekPersegiPanjangTanganKanan.panjang = 25;
        objekPersegiPanjangTanganKanan.lebar = 5;

        Lingkaran objekLingkaranTelapakTanganKiri = new Lingkaran();
        objekLingkaranTelapakTanganKiri.jari_jari = 2.5;

        Lingkaran objekLingkaranTelapakTanganKanan = new Lingkaran();
        objekLingkaranTelapakTanganKanan.jari_jari = 2.5;

        persegiPanjang objekPersegiPanjangKakiKiri = new persegiPanjang();
        objekPersegiPanjangKakiKiri.panjang = 10;
        objekPersegiPanjangKakiKiri.lebar = 30;

        persegiPanjang objekPersegiPanjangKakiKanan = new persegiPanjang();
        objekPersegiPanjangKakiKanan.panjang = 10;
        objekPersegiPanjangKakiKanan.lebar = 30;

        Lingkaran objekLingkaranTelapakKakiKiri = new Lingkaran();
        objekLingkaranTelapakKakiKiri.jari_jari = 5;
        objekLingkaranTelapakKakiKiri.diameter = 10;

        Lingkaran objekLingkaranTelapakKakiKanan = new Lingkaran();
        objekLingkaranTelapakKakiKanan.jari_jari = 5;
        objekLingkaranTelapakKakiKanan.diameter = 10;

        double  totalLuas = objekLingkaranKepala.getLuas() + objekPersegiPanjangBadan.getLuas() + objekPersegiPanjangTanganKiri.getLuas() +
                objekPersegiPanjangTanganKanan.getLuas() + objekLingkaranTelapakTanganKiri.getLuas() + objekLingkaranTelapakTanganKanan.getLuas() +
                objekPersegiPanjangKakiKiri.getLuas() + objekPersegiPanjangKakiKanan.getLuas() + objekLingkaranTelapakKakiKiri.getLuas() +
                objekLingkaranTelapakKakiKanan.getLuas();
        double  tinggiRobot = objekLingkaranKepala.diameter + objekPersegiPanjangBadan.lebar + objekPersegiPanjangKakiKiri.lebar +
                objekLingkaranTelapakKakiKiri.diameter;
        System.out.println("Luas Total Robot Tersebut = " +totalLuas);
        System.out.println("Tinggi Badan Robot Tersebut = " +tinggiRobot);
    }
}